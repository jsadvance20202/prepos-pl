import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import * as Thunk from 'redux-thunk';
import { act } from 'react-dom/test-utils';
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';

import reducer from '@main/__data__/reducers';

import Dashboard from '../dashboard';

configure({ adapter: new Adapter() });

const mockAnsver = async (mock, responses) => {

    // Match ALL requests
    await mock.onAny().reply((config) => {
        const [method, url, ...response] = responses.shift();
        if (config.url === url && config.method.toUpperCase() === method) {
            return response;
        }
        // Unexpected request, error out
        return [500, {}];
    });
}

describe('mount all app', () => {
    let mock

    beforeEach(() => {
        window.__webpack_public_path__ = '/';
        mock = new MockAdapter(axios);
    });

    it('mount', async () => {
        const app = mount(
            <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
                <Dashboard />
            </Provider>
        );
            
        const responses = [
            ['GET', '/api/getMainData', 200, {
                "status": {
                    "code": 0
                },
                mainData: {
                    ...require('../../../stubs/api/mainData/answer.json')
                }
              }
            ]
        ]

        await mockAnsver(mock, responses)

        app.update();
        mock.reset();
        expect(app).toMatchSnapshot();
    });


    // it('Работает без секции видео в ответе', async () => {
    //     const app = mount(
    //         <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
    //             <Dashboard />
    //         </Provider>
    //     );

    //     expect(app.find('App')).toMatchSnapshot();

    //     const responses = [
    //         ['GET', '/api/getMainData', 200, {
    //             "status": {
    //                 "code": 0
    //             },
    //             mainData: {
    //                 ...require('../../../stubs/api/mainData/killVideoSection.json')
    //             }
    //           }
    //         ]
    //     ]

    //     await mockAnsver(mock, responses)

    //     app.update();
    //     mock.reset();
    //     expect(app.find('App')).toMatchSnapshot();
    // });

    // it('show error on error', async () => {
    //     const app = mount(
    //         <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
    //             <Dashboard />
    //         </Provider>
    //     );

    //     const responses = [
    //         ['GET', '/api/getMainData', 404, {}]
    //     ]

    //     await mockAnsver(mock, responses)

    //     app.update();
    //     mock.reset();
    //     expect(app).toMatchSnapshot();
    // });
});
