import * as React from 'react';

import { Slide } from '../../model';
import { Wrapper, Dot, StyledArrow } from './styled';

/**
 * @prop {number} currentSlide Номер текущего слада.
 * @prop {Array<Slide>} slides Слайды.
 * @prop {Function} onPrevSlide Переключение слайда на предыдущий.
 * @prop {Function} onNextSlide Переключение слайда на следующий.
 * @prop {Function} goTo Переключение на слайда по индексу.
 */
interface SliderCounterProps {
    currentSlide: number;
    slides: Array<Slide>;
    onPrevSlide: () => void;
    onNextSlide: () => void;
    goTo: (index: number) => void;
}

// Компонент отвечающий за переключатель слайдов для слайдера.
export default class SliderCounter extends React.PureComponent<SliderCounterProps> {
    /**
     * Обработчик клика по точке.
     *
     * @param {React.SyntheticEvent<HTMLDivElement>} event Событие.
     */
    handleDotClick = (event: React.SyntheticEvent<HTMLDivElement>) => {
        const index = event.currentTarget.dataset.index;
        const { goTo } = this.props;

        goTo(Number(index));
    }

    /**
     * Рендер точек для перехода.
     *
     * @param {Slide} slide Слайд.
     * @param {number} index Индекс сайда.
     */
    renderDots = (slide: Slide, index: number) => {
        const { currentSlide } = this.props;

        return (
            <Dot selected={index === currentSlide} onClick={this.handleDotClick} key={`slide-${index}`} data-index={index} />
        );
    }

    render() {
        const { slides, onPrevSlide, onNextSlide } = this.props;

        return (
            <Wrapper>
                <StyledArrow data-cy="top-slider-arrow-left" size={32} onClick={onPrevSlide} />
                {slides.map(this.renderDots)}
                <StyledArrow data-cy="top-slider-arrow-right" flip={true} size={32} onClick={onNextSlide} />
            </Wrapper>
        );
    }
}
