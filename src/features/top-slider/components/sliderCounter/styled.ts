import styled from 'styled-components';

import { ArrowInSirlce } from '@main/assets';
import { device } from '@main/__data__/constants';

/**
 * @prop {boolean} selected Указывает выбран, ли данный элемент.
 */
interface DotProps {
    selected: boolean;
}

export const StyledArrow = styled(ArrowInSirlce)`
    opacity: .2;
    display: none;

    &:hover {
        opacity: 1;
    }

    @media ${device.tablet} {
        display: block;
    }
`;

export const Wrapper = styled.div`
    cursor: pointer;
    grid-column-start: 1;
    grid-column-end: 2;
    display: flex;
    align-items: center;
    justify-content: center;

    @media ${device.tablet} {
        grid-column-start: 1;
        grid-column-end: 3;
    }
`;

export const Dot = styled.div`
    cursor: pointer;
    width: 12px;
    height: 12px;
    border-radius: 50%;
    background-color: ${(props: DotProps) => props.selected ? '#003eff' : '#dbdbdb'};
    margin: 0 8px;

    @media ${device.tablet} {
        margin: 0 4px;
    }

    &:nth-child(2) {
        margin-left: 40px;
    }

    &:nth-last-child(2) {
        margin-right: 40px;
    }
`;
