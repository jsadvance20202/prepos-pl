import * as React from 'react';

import {
  LogoBlock,
  PhoneBlock,
  PhoneText,
  Wrapper,
  IconsBlock
} from './styled';
import { logoGrey as logoImg, phoneGrey, social } from '@main/assets';

interface SocialLinks {
  vk?: string;
  instagram?: string;
  ok?: string;
  facebook?: string;
  twitter?: string;
}

interface FooterProps {
    links: SocialLinks;
}

export const Footer = ({ links }: FooterProps) => (
  <Wrapper>
    <LogoBlock to={'/'}>
      <span>
        <img src={logoImg} alt="logo" />
      </span>
    </LogoBlock>
    <PhoneBlock>
      <div>
        <img src={phoneGrey} alt="phone" />
      </div>
      <PhoneText href="tel:+74952307071">8 (495) 230-70-71</PhoneText>
      <IconsBlock>
          {links.vk && <a target="_blank" href={links.vk}>
              <img src={social.gray.g_vk} alt="вконтакте"/>
          </a>}
          {links.instagram && <a href={links.instagram} target="_blank">
              <img src={social.gray.g_instagram} alt="instagram"/>
          </a>}
          {links.ok && <a href={links.ok} target="_blank">
              <img src={social.gray.g_ok} alt="Одноклассники"/>
          </a>}
          {links.facebook && <a href={links.facebook} target="_blank">
              <img src={social.gray.g_facebook} alt="facebook"/>
          </a>}
          {links.twitter && <a href={links.twitter} target="_blank">
              <img src={social.gray.g_twitter} alt="twitter"/>
          </a>}
      </IconsBlock>
    </PhoneBlock>
  </Wrapper>
);
