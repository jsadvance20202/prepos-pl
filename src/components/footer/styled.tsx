import styled from 'styled-components';
import { device } from '@main/__data__/constants';

export const Wrapper = styled.section`
    height: 136px;
    font-family: Montserrat;
    font-size: 12px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.17;
    letter-spacing: normal;
    color: #c2c2c2;
    display: flex;
    flex-direction: column;
    align-self: center;
    align-content: space-around;
    justify-content: center;
    padding: 0 20px;
    background-color: #f4f4f4;
    align-items: center;

    @media ${device.tablet} {
        height: 116px;
        flex-direction: row;
        justify-content: space-between;
    }
`;

export const LogoBlock = styled.a`
    align-self: center;
    text-decoration: none;
    margin-bottom: 18px;

    & img {
        height: 32px;
    }

    > div, a {
        align-self: center;
        text-decoration: none;

    }

    > a:hover, a:visited, div {
        text-decoration: none;
        color: #1e1d20;
    }

    @media ${device.tablet} {
        display: grid;
        grid-column-gap: 9px;
        grid-template-columns: 89px 205px;
        margin-bottom: 0;
    }
`;

export const PhoneBlock = styled.div`
    display: grid;
    grid-template-columns: 27px auto;
    grid-column-gap: 8px;

    > div {
      align-self: center;
    }

    img {
        width: 27px;
    }

    @media ${device.tablet} {
        grid-template-columns: 24px auto 242px;
    }
`;

export const PhoneText = styled.a`
    display: flex;
    align-items: center;
    text-decoration: none;
    font-size: 16px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: right;
    color: #c2c2c2;
`;

export const IconsBlock = styled.div`
    width: 100%;
    display: none;
    justify-content: center;
    align-items: center;

    a:nth-child(n+2) {
        margin-left: 16px;
    }

    @media ${device.tablet} {
        display: flex;
    }
`;
