import React, { PureComponent } from 'react';
import { PlaceHolder, StyledInput, Wrapper } from './styled';

export class TextInput extends PureComponent<any, any> {

    render() {
        const { placeholder, className, required, type, ...rest } = this.props;
        return (
            <Wrapper className={className}>
                <PlaceHolder required={required}>{placeholder}</PlaceHolder>
                <StyledInput {...rest} type={type || 'text'} required={required} onChange={(event) => this.props.onChange(event.currentTarget.value)} onBlur={(event) => this.props.onBlur(event.currentTarget.value)} />
            </Wrapper>
        );
    }
}
