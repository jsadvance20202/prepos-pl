import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { TextInput } from '../components/textinput';
import { Footer } from '../components/footer'
import { add } from '../simple';

configure({ adapter: new Adapter() });

describe('simple test', () => {
    it('just simple test check', () => {
        expect(true).toBe(true);
    })

    it('summ 2 numbers', () => {
        expect(add(1, 4)).toBe(5);
    })

    it('render text input', () => {
        const component = mount(<TextInput placeholder="просто плэйсхолдер" />);
        expect(component).toMatchSnapshot()
    })

    it('renders footer', () => {
        const component = mount(
            <Footer
                links={{
                    vk: '/',
                    instagram: '/',
                    ok: '/',
                    facebook: '/',
                    twitter: '/'
                }}
            />
        );
        expect(component).toMatchSnapshot()
    })
})