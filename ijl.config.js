const pkg = require('./package')
const path = require('path')

let publickPath = `/static/prepos/${process.env.VERSION || pkg.version}/`
if (process.env.ELECTRON) {
    publickPath = `${path.resolve(__dirname, 'dist')}/`
}

module.exports = {
    "apiPath": "stubs/api",
    "webpackConfig": {
        "output": {
            "publicPath": `/static/prepos/${process.env.VERSION || pkg.version}/`
        }
    },
    navigations: {
        "prepos": "/prepos"
    },
    apps: {
        prepos: { name: 'prepos', version: process.env.VERSION || pkg.version }
    },
    config: {
        "prepos.api.base": "/api"
    }
}