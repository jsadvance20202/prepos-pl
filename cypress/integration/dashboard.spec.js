describe('Simple testing', function() {
    it('just works', function () {
        cy.visit('/');

        cy.get('[data-cy="top-slider-arrow-left"]').should('have.length', 1).click().click().click()
        cy.get('[data-cy="top-slider-arrow-right"]').should('have.length', 1).click()

        cy.get('[data-cy="gallary-slider-arrow-left"]').should('have.length', 1).click().click().click()
        cy.get('[data-cy="gallary-slider-arrow-right"]').should('have.length', 1).click()

        cy.get('[data-cy="social-link"]').should('have.length', 5).as('socialLinks');

        cy.get('@socialLinks')
            .first()
                .should('have.attr', 'href')
                .and('include', 'vk.com')

        cy.get('@socialLinks')
            .eq(1)
            .then(function ($a) {
                // extract the fully qualified href property
                const href = $a.prop('href')
        
                // make an http request for this resource
                // outside of the browser
                cy.request(href)
                // drill into the response body
                .its('body')

                // and assert that its contents have the <html> response
                .should('include', '<title>')
                .and('include', '</html>')
              })
    });
});
