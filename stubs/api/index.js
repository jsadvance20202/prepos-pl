const router = require('express').Router()
const adminRoute = require('./admin')

router.get('/getMainData', (req, res) => {
    const adminEnv = process.env.ADMIN || 'answer'
    const answer = {
        "status": {
            "code": 0
        },
        mainData: require(`./mainData/${adminEnv}`)
    }

    if (adminEnv === 'error') {
        res.status(404).send({ status: { code: 1 }, error: require('./mainData/error') });
        return;
    }

    res.send(answer)
})

router.use(adminRoute)

module.exports = router
