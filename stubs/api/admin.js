const router = require('express').Router()

const SET_DATA_BASE_URL = '/setAdmin'

router.get(`${SET_DATA_BASE_URL}/:data`, (req, res) => {
    process.env.ADMIN = req.params.data

    if (req.params.data === 'reset')
        delete process.env.ADMIN

    res.send(`
    <h1>process.env.ADMIN установлен на ${process.env.ADMIN}</h1>
    `)
})

router.get('/admin', (req, res) => {
    const variants = [
        {
            url: `/api${SET_DATA_BASE_URL}/killVideoSection`,
            key: 'Убрать секцию с видео'
        }, {
            url: `/api${SET_DATA_BASE_URL}/error`,
            key: 'Ошибка'
        }, {
            url: `/api${SET_DATA_BASE_URL}/reset`,
            key: 'Сбросить'
        }
    ]

    res.send(`
<html>
    <head></head>
    <body>
        <h1>Тут можно поменять ответ  заглушки</h1>
        <ul>
            ${
                variants.map((variant) => `<li><button onclick="fetch('${variant.url}')">${variant.key}</button></li>`)
            }
        </ul>
    </body>
</html>
    `)
})

module.exports = router
